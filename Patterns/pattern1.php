<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pattern 1</title>
</head>
<body>
    <form action="#" method="post">
        <input type="number" name="userinput" id="userinput">
        <button type="submit">Submit</button>
    </form>
<?php
    if($_POST){
        $num = $_POST['userinput'];
        for ($i=0; $i < $num ; $i++) { 
            if(($num - $i)>=10){      // Loop to include spaces
                for($k = 0;$k<$i;$k++){
                    echo "&nbsp;&nbsp;";
                }
            }else{
                if($num >=10 ){
                    echo  str_repeat("&nbsp;&nbsp;",$num-9);
                }
            }
            for($j = -$num ; $j<= $num; $j++){
                if($j==0){
                    continue;
                }
                elseif(abs($j)<=$i){
                    print("*"); 
                }
                 else{
                    echo ($num-abs($j)+1);
                }
            }
            echo "<br>";
        }
        
    }
?>
</body>
</html>