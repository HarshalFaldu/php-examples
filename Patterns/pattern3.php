<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pattern 1</title>
</head>
<body>
    <form action="#" method="post">
        <input type="number" name="userinput" id="userinput">
        <button type="submit">Submit</button>
    </form>
<?php
    if($_POST){
        $num = $_POST['userinput'];
        for ($i=-$num; $i <= $num ; $i++) { 
            for($j = -($num+1) ; $j<= ($num+1); $j++){
                if($j==0){
                    continue;
                }elseif(abs($i)<abs($j)){
                    echo "*";
                }else{
                    echo "&nbsp;&nbsp;";
                }
            }
            echo "<br>";
        }
        
    }
?>
</body>
</html>