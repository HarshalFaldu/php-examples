<?php

$array = array(10,2,5,17,20,1);
print_r(mergeSort($array));


// Divide the array into two parts from the center element recursively until the array breaks into single element 
function mergeSort($array)
{
    if(count($array) == 1 )
    {
        return $array;
    }
    // Find the middle element
    $mid = count($array) / 2;

    // Store all the values that are at left side from mid point 
    $left = array_slice($array, 0, $mid);

    // Store all the values that are at right side from mid point
    $right = array_slice($array, $mid);
    
    // Again divide until the array have single element
    $left = mergeSort($left);
    $right = mergeSort($right);

    return merge($left, $right);
}

//Merge the element by comparing the elements in left and right array
function merge($left, $right)
{
    //New array to store sorted values
    $res = array();

    //Loops work for the 3 times if the left array have 4 values and right have 3( works for minimum length from both)
    while (count($left) > 0 && count($right) > 0)
    {
        //If value at 0th index in left is higher than the value at 0th index in right then append right value in res()
        if($left[0] > $right[0])
        {
            $res[] = $right[0];
            $right = array_slice($right , 1);
        }
        //Else append left value in res()
        else
        {
            $res[] = $left[0];
            $left = array_slice($left, 1);
        }
    }

    //Check if any element is there in left array then insert that element in res()
    while (count($left) > 0)
    {
        $res[] = $left[0];
        $left = array_slice($left, 1);
    }
    //Check if any element is there in right array then insert that element in res()
    while (count($right) > 0)
    {
        $res[] = $right[0];
        $right = array_slice($right, 1);
    }
    // Array Slice will remove that element from array
    return $res;
}

?>