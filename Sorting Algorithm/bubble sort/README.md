array = (10,2,5,17,20,1)

1st loop will traverse to all the array elements
for(i=0;i<len(arr);i++){

    2nd loop will traverse to all the element except last i elements because all last I elements are already sorted.
    for(j=0;j<len(arr)-i;j++){

        We need to swap the element j with j+1 if j+1 is smaller than j
        if(arr[j]>arr[j+1]){
            swap(j,j+1)
        }
    }
}

function swap(one,two){
    temp = one;
    one = two;
    two = temp;
}