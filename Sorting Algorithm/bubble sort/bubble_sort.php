<?php

$input_arr = array(10,2,5,17,20,1);

print_r(bubble_sort($input_arr));

function bubble_sort($array){
    for($i=0;$i<sizeof($array);$i++){
        for($j=0;$j<sizeof($array)-$i-1;$j++){  //Loop through all the element except last I index.
            if($array[$j] > $array[$j+1]){
                $temp = $array[$j];             // Swap the array elements at j and j+1 index
                $array[$j] = $array[$j+1];
                $array[$j+1] = $temp;
            }
        }
    }
    return $array;
}

?>