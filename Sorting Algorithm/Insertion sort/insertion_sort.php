<?php

$input_arr = array(10,2,5,17,20,1);

print_r(insertion_sort($input_arr));

function insertion_sort($arr){
    for ($i=1; $i < sizeof($arr); $i++) {       //Loop through all the elements of array except 1st
        $key = $arr[$i];         // Store key value which is used to check with other values.
        $j = $i-1;

        while($j >=0 && $arr[$j]>$key){ //Check if j is greater than or equal to 0(exiting condition) and also check weather key value is greater 
            $arr[$j + 1] = $arr[$j];    // than array value at J index or not
            $j = $j - 1;
        }

        $arr[$j+1] = $key;      //Insert minimum value(Key value) at j+1 index
    }
    return $arr;
}

?>