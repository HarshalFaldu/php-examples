<?php

$input_arr = array(10,2,5,17,20,1);

print_r(selection_sort($input_arr));

function selection_sort($arr){
    for ($i=0; $i < count($arr); $i++) {  //Loop through all the elements of array
        
        $min = $i;  

        // Loop through array elements after Ith index and find the smallest element after Ith index
        for ($j=$i+1; $j < count($arr); $j++) { 
            if($arr[$j] < $arr[$min]){
                $min = $j;
            }
        }

        //Swap the smallest element with Ith index;
        $temp = $arr[$min];
        $arr[$min] = $arr[$i];
        $arr[$i] = $temp;

    }
    return $arr;
}

?>