Worst Case & Average Case complexity O(n^2)

We are sorting the array by using PIVOT element(last element, first element or middle element) and we will divide the array recursively into two parts 
1) 1st array contains values less than pivot value
2) 2nd array contains values high than pivot value


selecting last value of array as pivot
array = (10,2,5,17,20,1);
print_r(quicksort(array),0,sizeof(array)-1);

function quicksort(arr,low,high){
    if(low<high){
        pivot = partition(arr,low,high);

        quicksort(arr,0,pivot-1);
        quicksort(arr,pivot+1,high);
    }
}


function partition(arr,low,high){
    initilize value at index high as pivot
    pivot = arr[high];
    i = (low-1); // inilize I as lower value

    Check if the value is lower than pivot, swap two values

    for(j=0;j<high-1;j++){
        if(arr[j]<=pivot){
            increment i by 1 value;
            swap(arr[i],arrr[j]);
        }
    }

    update the pivot element
    swap(arr[i+1],arr[high]);
    return the index of pivot
    return (i+1);
}