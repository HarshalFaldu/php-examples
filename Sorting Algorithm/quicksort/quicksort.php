<?php

$input_arr = array(10,2,5,20,17,1);
quicksort($input_arr,0,count($input_arr)-1); //quicksort(array,start,end)
print_r($input_arr);

// It will do partition with respect to the selected value(PIVOT) until the single element is there in array
function quicksort(&$arr,$low,$high){
    if($low<$high){
        $piv = partition($arr,$low,$high);
        quicksort($arr,$low,$piv-1); 
        quicksort($arr,$piv+1,$high);
    }
}

// Do the partition of the array with respect to pivot
function partition(&$arr,$low,$high){
    $pivot = $arr[$high]; //Here I have assigned last value of array as pivot
    $i = $low-1;
    for($j = $low;$j<=$high-1;$j++){  // Loop from low index to high index of array 
        if($arr[$j]<$pivot){ //check whether array element is less than pivot or not
            $i = $i+1;  
            $temp = $arr[$j];  // If condition is true than increment lower index by 1 and swap the value at index j with value
            $arr[$j] = $arr[$i];  //at index I
            $arr[$i] = $temp;
        }
    }
    $temp1 = $arr[$i+1];        //Update the pivot value
    $arr[$i+1] = $arr[$high];
    $arr[$high] = $temp1;
    return ($i+1);  //return the index of new pivot
}

?>