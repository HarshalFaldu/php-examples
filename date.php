<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Date</title>
</head>
<body>

    <form action="#" method="POST">
        <input type="date" name="date">
        <br>
        <br>
        <input type="date" name="date2">
        <button type="submit" name = "submit">Submit</button>
    </form>
    <?php
        echo date("Y-M-D")."<br>";
        echo date("y-m-d")."<br>";
        echo date("Y-m-d")."<br>";
        echo date("Y-m-d h:i:s")."<br>";
        echo date("Y-m-d H:i:s")."<br>";
        echo date("Y-m-d H:i:s a")."<br>";
        echo date("Y-m-d H:i:s A")."<br>";

        $d = mktime(10, 50, 59, 05, 10, 2000);
        echo date("Y/m/d H:i:s A",$d)."<br>";

        echo "<br>";
        $example = strtotime("10:15:59 10th May 2000");
        echo date("Y/m/d H:i:s A",$example)."<br>";

        echo "<br>";
        $example = strtotime("10th May 2000");
        echo date("Y/m/d H:i:s A",$example)."<br>";
        
        echo "<br>";
        $example = strtotime("May 2000");
        echo date("Y/m/d H:i:s A",$example)."<br>";   

        echo "<br>";
        $example = strtotime("Next day");
        echo "Next day : ".date("Y/m/d H:i:s A",$example)."<br>";

        echo "<br>";
        $example = strtotime("Saturday");
        echo "Next Saturday: ".date("Y/m/d H:i:s A",$example)."<br>";
        
        echo "<br>";
        $example = strtotime("2 months");
        echo "+2 Months: ".date("Y/m/d H:i:s A",$example)."<br>";

        echo "<br>";
        include("date_difference.php");
        echo "<br>";
        require("date_difference.php");

        ?>

</body>
</html>