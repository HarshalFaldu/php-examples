<?php

if($_POST){
    if(isset($_POST["submit"])){
        function isString ($value1){
            $regx = "/^[a-zA-Z ]*$/";
            if(!preg_match($regx,$value1)){
                throw new Exception("Not a Character");
            }
            return $value1;
        }
        
        try {
            echo isString($_POST["user__input"]);
        } catch (Exception $e) {
            echo  $e->getMessage();
        }
    }
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="#" method="post">
        <input type="text" name="user__input" id="user__input">
        <button type="submit" name="submit">Hit Enter</button>
    </form>
</body>
</html>