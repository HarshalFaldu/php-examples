<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>Registration</title>
</head>
<body>
    <?php include("submit_data.php"); ?>
    <div class="wrapper">
        <h1 >Registration</h1>
        <form action="#" method="post" enctype="multipart/form-data">
            First Name: <input type="text" name="fname" id="fname" value="<?= isset($fname) ? $fname : '';?>"><br>
            <span><?php echo $fname_error; ?></span>
            <br><br>
            Last Name:  <input type="text" name="lname" id="lname" value="<?= isset($lname) ? $lname : '';?>"><br>
            <span><?php echo $lname_error; ?><br><br></span>
            Contact Number: <input type="number" name="cnumber" id="cnumber" value="<?= isset($cnumber) ? $cnumber : '';?>"><br>
            <span><?php echo $cnumber_error; ?><br><br></span>
            Email id: <input type="email" name="email" id="email" value="<?= isset($email) ? $email : '';?>"><br>
            <span><?php echo $email_error; ?><br><br></span>
            Passwordd: <input type="password" name="pass" id="pass"><br>
            <span><?php echo $pass_error; ?><br><br></span>
            Upload Image: <input type="file" name="filetoupload" id="filetoupload"><br>
            <span><?php echo $upload_error; ?><br><br></span>
            <button type="submit">Sign Up</button>
        </form>
    </div>    
</body>
</html>