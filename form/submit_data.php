<?php
$fname_error = $lname_error = $cnumber_error = $email_error = $pass_error = $upload_error = $image_extenction = '';
$flag = 0;
    if($_POST){
        if(empty($_POST["fname"])){
            $fname_error = "Enter first name";
        }else{
            $fname = $_POST["fname"];
            if(!preg_match("/^[a-zA-Z ]*$/",$fname)){
                $fname_error = "Only characters and white spaces are allowed.";
            }
        }
        if(!empty($fname_error)){$flag = 1;}

        if(empty($_POST["lname"])){
            $lname_error = "Enter last name";
        }else{
            $lname = $_POST["lname"];
            if(!preg_match("/^[a-zA-Z ]*$/",$lname)){
                $lname_error = "Only characters and white spaces are allowed.";
            }
        }
        if(!empty($lname_error)){$flag = 1;}

        if(empty($_POST["cnumber"])){
            $cnumber_error = "Enter Number";
        }else{
            $cnumber = $_POST["cnumber"];
            if(!preg_match("/^[0-9]{10}$/",$cnumber)){
                $cnumber_error = "Enter exact 10 numbers";
            }
        }
        if(!empty($cnumber_error)){$flag = 1;}

        if(empty($_POST["email"])){
            $email_error = "Enter email";
        }else{
            $email = $_POST["email"];
            if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                $email_error = "Enter email with correct format";
            }
        }
        if(!empty($email_error)){$flag = 1;}

        if(empty($_POST["pass"])){
            $pass_error = "Enter Password";
        }else{
            $pass = $_POST["pass"];
            if(!preg_match("/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/",$pass)){
                $pass_error = "Have a password of length 8 and contains atleast one capital and one number";
            }
        }
        if(!empty($pass_error)){$flag = 1;}
        $uploadDone = 0;
        if($flag !== 1){
            $target_dir = "./uploads/";
            $target_file = $target_dir . basename($_FILES["filetoupload"]["name"]);
            $image_extenction = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            $check = getimagesize($_FILES["filetoupload"]["tmp_name"]);
            if($check!==false){
                
                if($image_extenction != "png"){
                    $upload_error = " Upload file with png extension";
                }else{
                    if($_FILES["filetoupload"]["size"]>100000){
                        $upload_error = "Upload file with size less than 100kb";
                    }else{
                        if(file_exists($target_file)){
                            $upload_error = "File already exists";
                        }
                        else{
                            move_uploaded_file($_FILES["filetoupload"]["tmp_name"],$target_file);
                            $uploadDone = 1;
                        }
                    }
                }
            }
            if(!empty($upload_error)){$flag=1;}
        }

        if($flag == 0 && $uploadDone==1){
            $data = json_encode($_POST);
            $open_file = fopen("data.txt","a") or die("Unable to open file");
            fwrite($open_file,$data.PHP_EOL);
            fclose($open_file);
            header("Location: login.php");
        }
    }
?>