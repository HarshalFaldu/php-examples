<?php
  
    $email_error = $pass_error = '';
    $flag = 0;
        if($_POST){
            if(empty($_POST["email"])){
                $email_error = "Enter email";
            }else{
                $email = $_POST["email"];
                if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                    $email_error = "Enter email with correct format";
                }
            }
            if(!empty($email_error)){$flag = 1;}
    
            if(empty($_POST["pass"])){
                $pass_error = "Enter Password";
            }else{
                $pass = $_POST["pass"];
                if(!preg_match("/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/",$pass)){
                    $pass_error = "Have a password of length 8 and contains atleast one capital and one number";
                }
            }
            if(!empty($pass_error)){$flag = 1;}
    
            if($flag==0){
                $fileopen = fopen("data.txt","r") or die("Unable to open");
                while(!feof($fileopen)){
                    $stri = fgets($fileopen);
                    $obj = json_decode($stri);
                    if($obj->email == $email && $obj->pass == $pass){
                        $_SESSION['email'] = $email;
                        setcookie("email",$email,time()+300);
                        header("Location: success.php");
                        break;
                    }
                }
                fclose($fileopen);
            }
        }

?>